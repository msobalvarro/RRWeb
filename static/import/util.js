const get = (e) => {
	return document.getElementById(e)
}

const getClass = (e) => {
	return [...document.getElementsByClassName(e)]
}

export default { get, getClass }