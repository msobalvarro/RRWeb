<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <title>Admin</title>
    <link rel="stylesheet" href="../static/css/materialize.min.css">
</head>

<body>
    <div class="navbar-fixed">
        <ul id="dropPublicaciones" class="dropdown-content">
            <li><a href="#!">Programas</a></li>
            <li><a href="#!">Eventos</a></li>
            <li class="divider"></li>
            <li><a href="#!">Noticieros</a></li>
        </ul>
         <ul id="dropPersonalizar" class="dropdown-content">
            <li><a href="#!">Top Musical</a></li>
            <li><a href="#!">Galeria</a></li>
            <li class="divider"></li>
            <li><a href="#!">Versiculo</a></li>
        </ul>
        <nav>
            <div class="nav-wrapper">
                <a href="#!" class="brand-logo">Logo</a>
                <ul class="right hide-on-med-and-down">
                    <li>
                        <a href="#" class="dropdown-button" data-activates="dropPublicaciones">Eventos</a>
                    </li>
                    <li>
                        <a href="#" class="dropdown-button" data-activates="dropPersonalizar">Noticiero</a>
                    </li>
                </ul>
            </div>
           <!--  <div class="nav-content">
                <span class="nav-title">Title</span>
                <a class="btn-floating btn-large halfway-fab waves-effect waves-light teal">
                    <i class="material-icons">add</i>
                </a>
            </div> -->
        </nav>
    </div>
    <div class="container">
    </div>
    <?php
        @session_start();
    
        if(isset($_SESSION['usuario']))
        {
            include 'paginas/index.html';            
        }
        else
        {
            echo "No existe un usuario";
        }
    ?>
        <script type="text/javascript" src="static/js/bundle.js"></script>
</body>

</html>