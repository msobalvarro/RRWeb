'use strict';

const gulp = require('gulp')
const babel = require('gulp-babel')
const uglify = require('gulp-uglify')
const buffer = require('vinyl-buffer')
const source = require('vinyl-source-stream')
const browserify = require('browserify')
const babelify = require('babelify')
const sourcemaps = require('gulp-sourcemaps')
const watchify = require('watchify')


function bundle() {
    return browserify({
            entries: 'static/index.js',
            transform: [babelify],
            debug: true
        })
        .transform(babelify, { presets: ["es2015", "es2017", "stage-0"] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('static/js'))
}

function admin() {
    return browserify({
            entries: 'admin/static/js/index.js',
            transform: [babelify],
            debug: true
        })
        .transform(babelify, { presets: ["es2015", "es2017", "stage-0"] })
        .bundle()
        .pipe(source('bundle.js'))
        .pipe(buffer())
        .pipe(sourcemaps.init({ loadMaps: true }))
        .pipe(uglify())
        .pipe(sourcemaps.write('./'))
        .pipe(gulp.dest('admin/static/js'))
}

gulp.task('bundle', function() {
    bundle()
    admin()
})

gulp.task('default', ['bundle'])