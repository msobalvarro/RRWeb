-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 25-01-2017 a las 18:33:39
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `rr`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `galeria`
--

CREATE TABLE IF NOT EXISTS `galeria` (
`id` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `titulo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `galeria`
--

INSERT INTO `galeria` (`id`, `imagen`, `titulo`) VALUES
(4, 'DSC01062-compressor.jpg', 'Doris'),
(6, 'DSC01081-compressor.jpg', 'ileana'),
(7, 'DSC01049-compressor.jpg', 'Marlon'),
(8, 'DSC01053-compressor.jpg', 'juan'),
(9, 'DSC01201-compressor.jpg', 'felix'),
(11, 'DSC01074-compressor.jpg', 'Desiree'),
(34, 'RRR-compressor.jpg', 'RRR'),
(35, 'EQUIPO-compressor.jpg', 'C'),
(36, 'Pastora.jpg', 'Pastora');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `noticias`
--

CREATE TABLE IF NOT EXISTS `noticias` (
`id` int(11) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `noticias`
--

INSERT INTO `noticias` (`id`, `texto`, `imagen`) VALUES
(2, '<h1>Encabezado</h1><hr /><blockquote><p>&nbsp;</p><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.<br />&nbsp;</p></blockquote><p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum iure alias, harum quas rem debitis provident perferendis aliquid maxime, dicta officiis accusamus laborum, ipsa possimus. Accusamus nobis quos, inventore modi.</p>', 'cold-melting-thawing-winter-1600x900.jpg'),
(3, '<p>Prensa</p>\r\n', 'screen.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacion`
--

CREATE TABLE IF NOT EXISTS `programacion` (
`id` int(11) NOT NULL,
  `programa` varchar(255) NOT NULL,
  `horario` varchar(255) NOT NULL,
  `portada` varchar(250) NOT NULL,
  `imagen1` varchar(250) NOT NULL,
  `imagen2` varchar(250) NOT NULL,
  `imagen3` varchar(250) NOT NULL,
  `segmento` text NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programacion`
--

INSERT INTO `programacion` (`id`, `programa`, `horario`, `portada`, `imagen1`, `imagen2`, `imagen3`, `segmento`) VALUES
(29, 'Atareder Ranchero Con Ricardo Ramirez', 'Lunes a Viernes de 6 a 7 PM', 'pagina wed radio-01-min.jpg', 'cuadros-informacion-02-compressor.jpg', 'Sin-titulo-1-03-compressor.jpg', 'cuadros-informacion-01-compressor.jpg', '<h1>Encabezado:</h1>\r\n\r\n<hr />\r\n<ul>\r\n	<li>Segementos</li>\r\n	<li>Segementos</li>\r\n	<li>Segementos</li>\r\n	<li>Segementos</li>\r\n	<li>Segementos</li>\r\n</ul>\r\n');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE IF NOT EXISTS `publicaciones` (
`id` int(11) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=40 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`id`, `texto`, `imagen`) VALUES
(37, '<h1>Encabezado</h1>\r\n\r\n<hr />\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.</p>\r\n\r\n<h2>&nbsp;</h2>\r\n\r\n<h2>Sub Encabezado</h2>\r\n\r\n<blockquote>\r\n<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aliquid incidunt culpa, iste quo quas commodi natus itaque voluptatum! Quasi, hic nulla sunt mollitia autem nihil officiis architecto modi, ut in.</p>\r\n</blockquote>\r\n', 'cold-melting-thawing-winter-1600x900.jpg'),
(38, '<h1>Nuestro eficiente equipo de Prensa.</h1>\r\n\r\n<hr />\r\n<p>Siempre trabajando de forma profesional.</p>\r\n', 'DSC01106.JPG'),
(39, '<p>yr5juys6tkjutkjtjhtjh</p>\r\n', 'banana_split2.jpg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
`id` int(11) NOT NULL,
  `radio` varchar(100) NOT NULL,
  `porcentaje` varchar(5) NOT NULL,
  `color` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating`
--

INSERT INTO `rating` (`id`, `radio`, `porcentaje`, `color`) VALUES
(1, 'La Nueva Ya', '0.590', '#8e44ad'),
(2, 'Stereo Lite ', '0.374', '#9b59b6'),
(3, 'Restauración', '0.297', '#e74c3c'),
(4, 'Maranatha', '0.297', '#9b59b6'),
(5, 'Stereo Mía ', '0.256', '#8e44ad'),
(6, 'Tropicálida', '0.252', '#9b59b6'),
(7, 'La Picosa ', '0.203', '#8e44ad'),
(8, 'Radio Futura ', '0.189', '#9b59b6'),
(9, 'Radio Hit ', '0.178', '#8e44ad'),
(10, 'Amor ', '0.168', '#9b59b6');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tewt`
--

CREATE TABLE IF NOT EXISTS `tewt` (
`id` int(11) NOT NULL,
  `codigo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tewt`
--

INSERT INTO `tewt` (`id`, `codigo`) VALUES
(4, '<h1>Samuel</h1>');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `texto_biblico`
--

CREATE TABLE IF NOT EXISTS `texto_biblico` (
`id` int(11) NOT NULL,
  `texto` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `texto_biblico`
--

INSERT INTO `texto_biblico` (`id`, `texto`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure'),
(2, 'Texto Biblico'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure'),
(4, 'versiculo biblico'),
(5, 'Nuevo Versiculo Bilbico'),
(6, 'Lucas 4:38-39\r\nY levantÃ¡ndose, {saliÃ³} de la sinagoga y entrÃ³ en casa de SimÃ³n. Y la suegra de SimÃ³n se hallaba sufriendo con una fiebre muy alta, y le rogaron por ella. E inclinÃ¡ndose sobre ella, reprendiÃ³ la fiebre, la dejÃ³; y al instante ella s');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `top`
--

CREATE TABLE IF NOT EXISTS `top` (
`id` int(11) NOT NULL,
  `radio` varchar(100) NOT NULL,
  `top` int(11) NOT NULL,
  `imagen` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `top`
--

INSERT INTO `top` (`id`, `radio`, `top`, `imagen`) VALUES
(4, 'Musica #1', 1, '3BB.jpg'),
(5, 'Musica #2', 2, '3BB.jpg'),
(12, 'musica #3', 3, '5IixVz9.png');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `clave`) VALUES
(3, 'msobalvarro', 'c2FtdWVs'),
(5, 'felix', 'MTIz'),
(6, 'RR1079', 'bnVtZXJvMQ==');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `galeria`
--
ALTER TABLE `galeria`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `noticias`
--
ALTER TABLE `noticias`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `programacion`
--
ALTER TABLE `programacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rating`
--
ALTER TABLE `rating`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tewt`
--
ALTER TABLE `tewt`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `texto_biblico`
--
ALTER TABLE `texto_biblico`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `top`
--
ALTER TABLE `top`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `galeria`
--
ALTER TABLE `galeria`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=37;
--
-- AUTO_INCREMENT de la tabla `noticias`
--
ALTER TABLE `noticias`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `programacion`
--
ALTER TABLE `programacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=30;
--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT de la tabla `rating`
--
ALTER TABLE `rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `tewt`
--
ALTER TABLE `tewt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT de la tabla `texto_biblico`
--
ALTER TABLE `texto_biblico`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `top`
--
ALTER TABLE `top`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
