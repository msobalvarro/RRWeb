-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 12-01-2017 a las 21:12:09
-- Versión del servidor: 5.6.21
-- Versión de PHP: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de datos: `rr`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `imagen`
--

CREATE TABLE IF NOT EXISTS `imagen` (
`id` int(11) NOT NULL,
  `nombre` varchar(255) NOT NULL,
  `tamano` varchar(255) NOT NULL,
  `tipo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `imagen`
--

INSERT INTO `imagen` (`id`, `nombre`, `tamano`, `tipo`) VALUES
(0, 'ZGVmYXVsdC5qcGc=', '', ''),
(14, 'bG9nby5qcGc=', '46803', 'image/jpeg'),
(22, 'b3BjaW9uIDIucG5n', '9619', 'image/png'),
(23, 'U2luIHTDrXR1bG8ucG5n', '279118', 'image/png'),
(24, 'ZmVjaGFzIGltcG9ydGFudGVzLmpwZw==', '145600', 'image/jpeg'),
(25, 'MTI5NzAtdGh1bWIuanBn', '59088', 'image/jpeg'),
(26, 'MTMzOTIwOThfMTE0NzY0MTIwODYxMzEzNV81NTc2NzUwMTkwMzczMDg2OTdfby5qcGc=', '330538', 'image/jpeg');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `programacion`
--

CREATE TABLE IF NOT EXISTS `programacion` (
`id` int(11) NOT NULL,
  `programa` varchar(255) NOT NULL,
  `horario` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `programacion`
--

INSERT INTO `programacion` (`id`, `programa`, `horario`) VALUES
(4, 'BENDICION DE DIOS (PASTOR OVIDIO â€“ PROGRAMA DE CONSEJERIA)', 'LUNES A VIERNES 9-10AM'),
(10, 'UN MOMENTO DE REFLEXIÃ“N ( PASTOR OVIDIO ) PREDICACION', 'LUNES A SABADO  8AM / 1PM  /  11:30PM DOMINGO   11:30PM'),
(11, 'EN FAMILIA (PASTORA AURITA â€“ PROGRAMA DE REFLEXIÃ“N DIRIGIDO A LA FAMILIA)', 'VIERNES 12 â€“ 12:30MD'),
(12, 'UN NUEVO DÃA (COMPLACENCIAS MUSICALES Y SALUDOS)', 'LUNES A VIERNES 6:30 â€“ 7:00 AM SABADOS 6:00 â€“ 7:00AM'),
(13, 'TIEMPO DE BENDICION Y GLORIA (COMPLACENCIAS MUSICALES, SALUDOS, INFORMACIÃ“N, CURIOSIDADES)', 'LUNES A SABADO  10AM â€“ 12MD  /  7PM â€“ 9PM DOMINGO  7PM â€“ 9PM'),
(14, 'TARDE FESTIVA (COMPLACENCIAS MUSICALES, SALUDOS, PENSAMIENTOS, CONSEJO FINANCIERO)', 'LUNES A VIERNES 2:30 â€“ 4PM'),
(15, 'LOCUCIÃ“N EN LINEA (CADA OYENTE SE CONVIERTE EN LOCUTOR AL PRESENTAR LA MÃšSICA DE SU PREFERENCIA)', 'LUNES A VIERNES 4 â€“ 5PM'),
(16, 'ATARDECER RANCHERO (PROGRAMA DEDICADO A LOS APASIONADOS POR LA MUSICA RANCHERA, LO DIRIGE â€œPANFILO Y EL ABUELO CIPRIANOâ€)', 'LUNES A VIERNES 6- 7PM'),
(17, 'FIESTA PA AMANECER (INICIO DE NUESTRA PROGRAMACION COMPLACENCIAS MUSICALES, SALUDOS, REFLEXIONES, CURIOSIDADES)', 'LUNES A DOMINGO 12 - 2AM'),
(18, 'EXPLOSIÃ“N INFANTIL  (45 MINUTOS DEDICADO A LOS NIÃ‘OS, COMPLACENCIAS MUSICALES, SALUDOS, INFORMACIÃ“N BÃBLICA Y MÃS)', 'SABADOS 9 â€“ 9:45AM'),
(19, 'EL REVENTON ( 3 HORAS DE SALUDOS, COMPLACENCIAS MUSICALES, PROGRAMACION DE MUSICA AL INSTANTE, RECOMENDACIONES MUSICALES, CHISTES Y MAS)', 'DOMINGOS 3 â€“ 6PM'),
(20, 'NOTICIERO RESTAURACIÃ“N (TU CONTACTO DIRECTO CON LA INFORMACIÃ“N)', 'LUNES A VIERNES  AUDICION MATUTINA 6AM -  AUDICION ESTELAR 5PM BOLETINES INFORMATIVOS  9 â€“ 10 -11 AM / 3 â€“ 4 PM / 9 PM SABADOS RESUMEN FIN DE SEMANA 3PM'),
(21, 'EL ALTAR DE LA ORACIÃ“N ( PETICIONES Y ORACIONES)', 'LUNES A DOMINGOS: 3:00 / 4:00 AM');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `publicaciones`
--

CREATE TABLE IF NOT EXISTS `publicaciones` (
`id` int(11) NOT NULL,
  `titulo` varchar(255) NOT NULL,
  `subtitulo` varchar(255) NOT NULL,
  `modelo` varchar(255) NOT NULL,
  `texto` text NOT NULL,
  `imagen` varchar(255) NOT NULL,
  `id_imagen` int(11) DEFAULT NULL,
  `fecha` varchar(100) NOT NULL,
  `importante` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `publicaciones`
--

INSERT INTO `publicaciones` (`id`, `titulo`, `subtitulo`, `modelo`, `texto`, `imagen`, `id_imagen`, `fecha`, `importante`) VALUES
(15, 'Punto Economico', 'Nueva empresa', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dol', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'true', 14, '2016-08-10T05:00:15.321Z', ''),
(24, 'Evento sin imagen4', 'Estas invitado', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmodtempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodoconsequat. Duis aute irure dol', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\ntempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\nquis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\nconsequat. Duis aute irure dolor in reprehenderit in voluptate velit esse\r\ncillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non\r\nproident, sunt in culpa qui officia deserunt mollit anim id est laborum.', 'true', 22, '2016-08-10T05:00:15.321Z', ''),
(26, 'Evento', 'Subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure ', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure dol', 'false', 0, '2016-08-10T05:00:15.321Z', ''),
(27, 'asdf', 'asdf', 'nasdf', 'asdf', 'true', 23, '2016-08-10T05:00:15.321Z', 'importante'),
(28, 'Fechas Importantes', 'Importante', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure', 'true', 24, '2016-08-10T05:00:15.321Z', 'importante'),
(29, 'Prueba Fecha', 'Fecha inicial', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure', 'false', 0, '2016-08-10T05:00:15.321Z', ''),
(30, 'Evento Prueba', 'Subtitulo', 'Nada que mostrar', 'Nada que mostrar.....', 'false', 0, '2016-08-10T05:21:57.736Z', ''),
(31, 'Prueba de vento2', 'Subtitulo', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure', 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure\r\n\r\nLorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure', 'false', 0, '2016-08-16T20:22:22.712Z', '');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `rating`
--

CREATE TABLE IF NOT EXISTS `rating` (
`id` int(11) NOT NULL,
  `radio` varchar(100) NOT NULL,
  `porcentaje` varchar(5) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `rating`
--

INSERT INTO `rating` (`id`, `radio`, `porcentaje`) VALUES
(1, 'La Nueva Ya', '0.590'),
(2, 'Stereo Lite ', '0.374'),
(3, 'Restauración', '0.297'),
(4, 'Maranatha', '0.297'),
(5, 'Stereo Mía ', '0.256'),
(6, 'Tropicálida', '0.252'),
(7, 'La Picosa ', '0.203'),
(8, 'Radio Futura ', '0.189'),
(9, 'Radio Hit ', '0.178'),
(10, 'Amor ', '0.168');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tewt`
--

CREATE TABLE IF NOT EXISTS `tewt` (
`id` int(11) NOT NULL,
  `codigo` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `tewt`
--

INSERT INTO `tewt` (`id`, `codigo`) VALUES
(1, '<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FOnLine1079%2Fposts%2F1736731859947748&width=500" height="511" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe'),
(2, '<iframe src="https://www.facebook.com/plugins/post.php?href=https%3A%2F%2Fwww.facebook.com%2FOnLine1079%2Fphotos%2Fa.1418311255123145.1073741825.1418310465123224%2F1728039270817007%2F%3Ftype%3D3&width=500" height="156" style="border:none;overflow:hidden" ');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `texto_biblico`
--

CREATE TABLE IF NOT EXISTS `texto_biblico` (
`id` int(11) NOT NULL,
  `texto` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `texto_biblico`
--

INSERT INTO `texto_biblico` (`id`, `texto`) VALUES
(1, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure'),
(2, 'Texto Biblico'),
(3, 'Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod\r\n tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,\r\n quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo\r\n consequat. Duis aute irure'),
(4, 'versiculo biblico'),
(5, 'Nuevo Versiculo Bilbico'),
(6, 'Lucas 4:38-39\r\nY levantÃ¡ndose, {saliÃ³} de la sinagoga y entrÃ³ en casa de SimÃ³n. Y la suegra de SimÃ³n se hallaba sufriendo con una fiebre muy alta, y le rogaron por ella. E inclinÃ¡ndose sobre ella, reprendiÃ³ la fiebre, la dejÃ³; y al instante ella s');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `usuarios`
--

CREATE TABLE IF NOT EXISTS `usuarios` (
`id` int(11) NOT NULL,
  `usuario` varchar(255) NOT NULL,
  `clave` varchar(255) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `usuarios`
--

INSERT INTO `usuarios` (`id`, `usuario`, `clave`) VALUES
(1, 'msobalvarro', 'c2FtdWVs');

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `imagen`
--
ALTER TABLE `imagen`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `programacion`
--
ALTER TABLE `programacion`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `rating`
--
ALTER TABLE `rating`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tewt`
--
ALTER TABLE `tewt`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `texto_biblico`
--
ALTER TABLE `texto_biblico`
 ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `usuarios`
--
ALTER TABLE `usuarios`
 ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `imagen`
--
ALTER TABLE `imagen`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=27;
--
-- AUTO_INCREMENT de la tabla `programacion`
--
ALTER TABLE `programacion`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT de la tabla `publicaciones`
--
ALTER TABLE `publicaciones`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=32;
--
-- AUTO_INCREMENT de la tabla `rating`
--
ALTER TABLE `rating`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT de la tabla `tewt`
--
ALTER TABLE `tewt`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT de la tabla `texto_biblico`
--
ALTER TABLE `texto_biblico`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=7;
--
-- AUTO_INCREMENT de la tabla `usuarios`
--
ALTER TABLE `usuarios`
MODIFY `id` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=2;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
